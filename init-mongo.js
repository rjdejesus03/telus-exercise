// Select DB
db = db.getSiblingDB('local')

// Create Collection
db.createCollection('subscribers')

// Create Index
db.subscribers.createIndex({ "phoneNumber": 1 }, { unique: true })

// Insert Data
db.subscribers.insertOne(
    {
        phoneNumber: "18675181010",
        username: "16045906403",
        password: "p@ssw0rd!",
        domain: "ims.mnc660.mcc302.3gppnetwork.org",
        status: "ACTIVE",
        features: {
            callForwardNoReply: {
                provisioned: true,
                destination: "tel:+18675182800"
            }
        }
    }

)