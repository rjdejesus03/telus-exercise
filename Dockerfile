FROM node:17.3.0

WORKDIR /usr/src/app
COPY package*.json ./
COPY gulpfile.js ./
COPY tsconfig*.json ./
COPY .env ./
COPY ./src ./src

RUN npm install
RUN npm run build

EXPOSE 8080
CMD [ "node", "./build/server.js" ]