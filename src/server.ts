import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as morgan from "morgan";
import * as http from "http";
import * as dotenv from "dotenv";
import * as swaggerUi from "swagger-ui-express";
import * as mongoose from "mongoose";
import apiDoc from "./docs/api-doc";
import routes from "./routes";

class HttpServer {
  public app: express.Application;

  public static bootstrap(): HttpServer {
    return new HttpServer();
  }

  constructor() {
    this.app = express.default();
    this.serverConfig();
    this.dbConfig();
    this.routes();
    this.apiDocs();
  }

  private serverConfig() {
    dotenv.config();
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
    this.app.use(cors.default());
    this.app.use(morgan.default("dev"));
    this.app.use(
      (
        err: any,
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ) => {
        var error = new Error("Not Found");
        err.status = 404;
        next(err);
      }
    );
  }

  private dbConfig() {
    try {
      mongoose.connect(process.env.MONGODB_CONNECTION_STRING || "")
    } catch (error) {
      console.error(error);
      process.exit(1);
    }

  }

  private routes() {
    this.app.use("/api/ims", routes);
  }

  private apiDocs() {
    this.app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(apiDoc));
  }
}

const onListening = () => {
  var addr = server.address();
  var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr?.port;
  console.log("Listening on " + bind);
};

const onError = (error: any) => {
  if (error.syscall !== "listen") {
    throw error;
  }

  var bind = typeof port === "string" ? "Pipe " + port : "Port " + port;
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const port: any = process.env.PORT || 8080;
let app = HttpServer.bootstrap().app;
app.set("port", port);

const server = http.createServer(app);
server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

export default server;
