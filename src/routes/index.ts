import SubscribersRoute from "./subscribers-route";
import { Router } from "express";

const routes = Router();

routes.use("/", SubscribersRoute);

export default routes;
