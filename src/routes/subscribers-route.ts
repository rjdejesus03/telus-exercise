import { Router } from "express";
import SubscribersController from "../controller/subscribers-controller";
const SubscribersRoute = Router();

//User
SubscribersRoute.get("/subscriber/:phoneNumber", SubscribersController.getSubscriber);
SubscribersRoute.put("/subscriber/:phoneNumber", SubscribersController.updateSubscriber);
SubscribersRoute.delete("/subscriber/:phoneNumber", SubscribersController.deleteSubscriber);

export default SubscribersRoute;
