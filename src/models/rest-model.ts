export interface ResponseFormat {
  result?: any;
  code: number;
}
