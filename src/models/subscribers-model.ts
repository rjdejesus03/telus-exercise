import { ISubscriber } from "../types/subscriber-types"
import { model, Schema } from "mongoose";

const SubscriberSchema: Schema = new Schema(
  {
    phoneNumber: {
      type: String,
      required: true,
    },
    username: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    domain: {
      type: String,
    },
    status: {
      type: String,
    },
    feature: {
      callForwardNoReply: {
        provisioned: {
          type: Boolean,
        },
        destination: {
          type: String,
        },
      }
    }
  },
  { timestamps: true }
);

export default model<ISubscriber>("Subscriber", SubscriberSchema);