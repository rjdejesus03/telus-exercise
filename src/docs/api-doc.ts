const apiDoc = {
  openapi: "3.0.3",
  info: {
    title: "Telus",
    description: "Telus Code Exercise",
    termsOfService: "http://swagger.io/terms/",
    contact: {
      email: "rj.dejesus03@yahoo.com",
    },
    version: "1.0.0",
  },
  servers: [
    {
      url: "http://localhost:8080/api/ims",
    },
  ],
  tags: [
    {
      name: "subscriber",
      description: "Operations about subscriber",
    },

  ],
  paths: {
    "/subscriber/{phoneNumber}": {
      get: {
        tags: ["subscriber"],
        summary: "Get subscriber by phone number",
        operationId: "getSubscriber",
        parameters: [
          {
            name: "phoneNumber",
            in: "path",
            description: "The phone number of subscriber that needs to be fetched.",
            required: true,
            schema: {
              type: "string",
            },
          },
        ],
        responses: {
          "200": {
            description: "successful operation",
            content: {
              "application/json": {
                schema: {
                  $ref: "#/components/schemas/Subscriber",
                },
              },
            },
          },
          "404": {
            description: "Subscriber not found",
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  properties: {
                    code: {
                      type: "integer",
                      format: "int64",
                      example: 404,
                    },
                    result: {
                      type: "string",
                      example: "No subscriber found!",
                    },
                  }
                },
              },
            },
          },
        },
      },
      put: {
        tags: ["subscriber"],
        summary: "Upsert subscriber by phone number",
        operationId: "updateSubscriber",
        parameters: [
          {
            name: "phoneNumber",
            in: "path",
            description: "The phone number of subscriber that needs to be updated.",
            required: true,
            schema: {
              type: "string",
            },
          },
        ],
        requestBody: {
          description: "Subscriber details",
          required: true,
          content: {
            "application/json": {
              schema: {
                $ref: "#/components/schemas/SubscriberBody",
              },
            },
          },
        },
        responses: {
          "200": {
            description: "successful operation",
            content: {
              "application/json": {
                schema: {
                  $ref: "#/components/schemas/Subscriber",
                },
              },
            },
          },
          "404": {
            description: "Subscriber not found",
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  properties: {
                    code: {
                      type: "integer",
                      format: "int64",
                      example: 404,
                    },
                    result: {
                      type: "string",
                      example: "No subscriber found!",
                    },
                  }
                },
              },
            },
          },
        },
      },
      delete: {
        tags: ["subscriber"],
        summary: "Delete subscriber by phone number",
        operationId: "deleteSubscriber",
        parameters: [
          {
            name: "phoneNumber",
            in: "path",
            description: "The phone number of subscriber that needs to be deleted.",
            required: true,
            schema: {
              type: "string",
            },
          },
        ],
        responses: {
          "200": {
            description: "successful operation",
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  properties: {
                    code: {
                      type: "integer",
                      format: "int64",
                      example: 200,
                    },
                    result: {
                      type: "string",
                      example: "18675181010 successfully deleted!",
                    },
                  }
                },
              },
            },
          },
          "404": {
            description: "Subscriber not found",
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  properties: {
                    code: {
                      type: "integer",
                      format: "int64",
                      example: 404,
                    },
                    result: {
                      type: "string",
                      example: "No subscriber found!",
                    },
                  }
                },
              },
            },
          },
        },
      },
    },
  },
  components: {
    schemas: {
      Subscriber: {
        type: "object",
        properties: {
          code: {
            type: "integer",
            format: "int64",
            example: 200,
          },
          result: {
            type: "object",
            properties: {
              id: {
                type: "integer",
                format: "int64",
                example: 1,
              },
              phoneNumber: {
                type: "string",
                example: "18675181010",
              },
              username: {
                type: "string",
                example: "16045906403",
              },
              password: {
                type: "string",
                example: "p@ssw0rd",
              },
              domain: {
                type: "string",
                example: "ims.mnc660.mcc302.3gppnetwork.org",
              },
              status: {
                type: "string",
                example: "ACTIVE",
              },
              features: {
                type: "object",
                properties: {
                  callForwardNoReply: {
                    type: "object",
                    properties: {
                      provisioned: {
                        type: "boolean",
                        example: true,
                      },
                      destination: {
                        type: "string",
                        example: true,
                      }
                    }
                  }
                }
              }
            }
          },
        },
      },
      SubscriberBody: {
        type: "object",
        properties: {
          username: {
            type: "string",
            example: "16045906403",
          },
          password: {
            type: "string",
            example: "p@ssw0rd",
          },
          domain: {
            type: "string",
            example: "ims.mnc660.mcc302.3gppnetwork.org",
          },
          status: {
            type: "string",
            example: "ACTIVE",
          },
          features: {
            type: "object",
            properties: {
              callForwardNoReply: {
                type: "object",
                properties: {
                  provisioned: {
                    type: "boolean",
                    example: true,
                  },
                  destination: {
                    type: "string",
                    example: true,
                  }
                }
              }
            }
          }
        }
      },
    },
  },
};

export default apiDoc;
