import { Request, Response, NextFunction, query } from "express";
import { ISubscriber } from "../types/subscriber-types";
import SubscribersService from "../services/subscribers-service";

class SubscribersController {
  async getSubscriber(req: Request, res: Response) {
    try {
      let phoneNumber: any = req.params.phoneNumber;
      let response = await SubscribersService.getSubscriber(phoneNumber);
      return res.status(200).json(response);
    } catch (e: any) {
      res.status(500).json(e.message);
    }
  }

  async updateSubscriber(req: Request, res: Response) {
    try {
      let body: ISubscriber[] = req.body;
      let phoneNumber: any = req.params.phoneNumber;
      let response = await SubscribersService.updateSubscriber(phoneNumber, body);
      return res.status(200).json(response);
    } catch (e: any) {
      res.status(500).json(e.message);
    }
  }

  async deleteSubscriber(req: Request, res: Response) {
    try {
      let phoneNumber: any = req.params.phoneNumber;
      let response = await SubscribersService.deleteSubscriber(phoneNumber);
      return res.status(200).json(response);
    } catch (e: any) {
      res.status(500).json(e.message);
    }
  }
}

export default new SubscribersController();
