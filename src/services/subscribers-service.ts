import { ResponseFormat } from "../models/rest-model";
import { ISubscriber } from "../types/subscriber-types";
import Subscriber from "../models/subscribers-model";

class SubscribersService {
  public async getSubscriber(phoneNumber: String): Promise<any> {
    const res: ResponseFormat = { code: 200 };
    try {
      const response: ISubscriber[] = await Subscriber.findOne({ phoneNumber: phoneNumber });
      if (response) {
        res.result = response;
      } else {
        res.result = "No subscriber found!";
      }

      return res;
    } catch (e: any) {
      res.code = 500;
      res.result = e.message;
    }
  }

  public async updateSubscriber(phoneNumber: String, body: ISubscriber[]): Promise<any> {
    const res: ResponseFormat = { code: 200 };
    try {
      const filter = { phoneNumber: phoneNumber };
      const response = await Subscriber.findOneAndUpdate(filter, body, { upsert: true });
      console.log(response);
      if (response == null) {
        res.result = "Subscriber added successfully!";
      } else {
        res.result = response;
      }

      return res;
    } catch (e: any) {
      res.code = 500;
      res.result = e.message;
    }
  }

  public async deleteSubscriber(phoneNumber: String): Promise<any> {
    const res: ResponseFormat = { code: 200 };
    try {
      const response = await Subscriber.deleteOne({ phoneNumber: phoneNumber });

      if (response.deletedCount !== 0) {
        res.result = `${phoneNumber} deleted successfully!`;
      } else {
        res.result = "No subscriber found!";
      }

      return res;
    } catch (e: any) {
      res.code = 500;
      res.result = e.message;
    }
  }
}

export default new SubscribersService();
