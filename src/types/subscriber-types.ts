import { Document } from "mongoose";

export interface ISubscriber extends Document {
  phoneNumber: string;
  username: string;
  password: string;
  domain: string;
  status: string;
  feature: {
    callForwardNoReply: {
        provisioned: boolean;
        destination: string
    }
  }
}