const gulp = require("gulp");
const clean = require("gulp-clean");
const ts = require("gulp-typescript");
const eslint = require("gulp-eslint");
const tsProject = ts.createProject("tsconfig.json");
const BUILD = "build/";

// Lint task
gulp.task("lint", function () {
  return gulp.src("src/**/*.js").pipe(eslint({})).pipe(eslint.format());
});

// Clean build folder
gulp.task("clean", function () {
  return gulp.src(`${BUILD}*`, { read: false }).pipe(clean());
});

// Build json files
gulp.task("sourceJSON", () => {
  return gulp.src("src/*/**.json").pipe(gulp.dest(BUILD));
});

// Task which would transpile typescript to javascript
gulp.task("build", function () {
  return tsProject.src().pipe(tsProject()).js.pipe(gulp.dest(BUILD));
});

gulp.task(
  "default",
  gulp.series("lint", "clean", "sourceJSON", "build"),
  () => {
    console.log("Done");
  }
);
