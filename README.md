# telus-exercise

Telus coding exercise that composed up a dockerized API server using Express framework and a MongoDB.

## USAGE

##### Clone the repostiory

```
git clone https://gitlab.com/rjdejesus03/telus-exercise.git
```

##### Run Docker Compose

```
docker compose build
docker compose up -d
```

## PREREQUISITES

- Docker / Rancher Desktop

## API Documentation

Go to http://localhost:8080/api-doc. Powered by Open API specifications.

## More Information

- Security - I implemented some security measures like cors. For API Authentication, we can implement JWT or OAuth depends on client needs. But for this project, no authentication added.
- Documentation - The API is properly documented using OpenAPI specification
- Database - I used dockerized MongoDB and created an init-monggo.js to initialize data in database.
- Testing - I used Mocha and Chai. We can use postman testing as well. For local testing, don't forget to update env file and change host to localhost.
